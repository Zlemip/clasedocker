










AUI.add(
	'portal-available-languages',
	function(A) {
		var available = {};

		var direction = {};

		

			available['ca_ES'] = 'catal�n (Espa�a)';
			direction['ca_ES'] = 'ltr';

		

			available['zh_CN'] = 'chino (China)';
			direction['zh_CN'] = 'ltr';

		

			available['en_US'] = 'ingl�s (Estados Unidos)';
			direction['en_US'] = 'ltr';

		

			available['fi_FI'] = 'fin�s (Finlandia)';
			direction['fi_FI'] = 'ltr';

		

			available['fr_CA'] = 'franc�s (Canad�)';
			direction['fr_CA'] = 'ltr';

		

			available['de_DE'] = 'alem�n (Alemania)';
			direction['de_DE'] = 'ltr';

		

			available['iw_IL'] = 'hebreo (Israel)';
			direction['iw_IL'] = 'rtl';

		

			available['hu_HU'] = 'h�ngaro (Hungr�a)';
			direction['hu_HU'] = 'ltr';

		

			available['ja_JP'] = 'japon�s (Jap�n)';
			direction['ja_JP'] = 'ltr';

		

			available['pt_BR'] = 'portugu�s (Brasil)';
			direction['pt_BR'] = 'ltr';

		

			available['es_ES'] = 'espa�ol (Espa�a)';
			direction['es_ES'] = 'ltr';

		

		Liferay.Language.available = available;
		Liferay.Language.direction = direction;
	},
	'',
	{
		requires: ['liferay-language']
	}
);