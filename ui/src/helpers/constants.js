export const serviceUrl = 'http://52.27.16.61:19990';

const siteName = 'Nuevo Sitio 3';

export const SITES_URL = `${serviceUrl}/sites/${siteName}`;

export const EVENTS_URL = `${serviceUrl}/events`;
