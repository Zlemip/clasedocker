var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var port = 8090;
var _ = require('lodash');
var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
  host: 'elasticsearch:9200'
});

var active = false;

const stopwords = ["de","del","el","un","una","and","unas","unos","uno","sobre","todo","también","tras","otro","algún","alguno","alguna","algunos","algunas","ser","es","soy","eres","somos","sois","estoy","esta","estamos","estais","estan","como","en","para","atras","porque","por qué","estado","estaba","ante","antes","siendo","ambos","pero","por","poder","puede","puedo","podemos","podeis","pueden","fui","fue","fuimos","fueron","hacer","hago","hace","hacemos","haceis","hacen","cada","fin","incluso","primero desde","conseguir","consigo","consigue","consigues","conseguimos","consiguen","ir","voy","va","vamos","vais","van","vaya","gueno","ha","tener","tengo","tiene","tenemos","teneis","tienen","el","la","lo","las","los","su","aqui","mio","tuyo","ellos","ellas","nos","nosotros","vosotros","vosotras","si","dentro","solo","solamente","saber","sabes","sabe","sabemos","sabeis","saben","ultimo","largo","bastante","haces","muchos","aquellos","aquellas","sus","entonces","tiempo","verdad","verdadero","verdadera cierto","ciertos","cierta","ciertas","intentar","intento","intenta","intentas","intentamos","intentais","intentan","dos","bajo","arriba","encima","usar","usas","usa","usamos","usais","usan","emplear","muy","era","eras","eramos","eran","modo","bien","cual","cuando","donde","mientras","quien","con","entre","sin","podria","podrias","podriamos","podrian","podriais","yo","aquel"];

const server = global.server = express();
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
server.use(cookieParser());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());



server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

server.use(express.static('./static'));

server.get('/',function(req,res){
  res.status(200).json({message: 'Sistema de busqueda CGR'})
})

server.get('/frecuentes', function(req,res){
  client.search({
    index: 'els_cgr_config',
    type: 'frecuentes',
    query: {
      match_all: {}
    }
  }, function(err,res_in){
    var result  = [];
    if(res_in.hits){
       result = res_in.hits.hits.map(function(i){
        return {id: i._id, palabra: i._source.palabra}
      })
    }

    res.json({palabras:result.map(function(i){return i.palabra;})});
  })
})

server.post('/suggest/*',
  function(req, res){
    if(req.params[0] == 'contable'){
    client.suggest({
    index: 'cgr',
    body: {
      mysuggester: {
        text: req.body.search,

        term: {
          field: 'texto',
          size: 3
        },

      }
    }
    }, function (error, response) {
        res.json(response);
    })
  } else {
    client.suggest({
    index: 'cgr',
    body: {
      mysuggester: {
        text: req.body.search,

        term: {
          field: 'texto',
          size: 3,
          analyzer: 'spanish'
        },

      }
    }
    }, function (error, response) {
        res.json(response);
    })
  }
})

server.get('/byid/:fuente/:id',
  function(req,res){
      client.get({
        index: 'cgr',
        type: req.params.fuente,
        id: req.params.id
      }, function (error, response) {
      //  console.log(response);
          res.json(response);
      });
  }
)
server.post('/external/:func',
  function(req,res){
    //console.log(req.body);
    var  search_res = pruneEmpty({
      index: 'externa',
      type: req.body.index_type,
      size: 200,

      body: {

        query: {
          match_all: {}
        }
      }
    });
    client.search(search_res,function(err,resp){
        //console.log(JSON.stringify(search_res,null,4));
        console.log(err);
        res.json(resp)
      })

  }
)

server.get('/dinamic/:fuente/:source', function(req,res){

    client.search({
      index: 'cgr',
      type: req.params.fuente,
      body: {
        "query": {
            "match_all": {}
        },
        aggs: {
          "distinct": {
            terms: {
              field: req.params.source,
              size: 1000
            }
          }
        }
      }
    }, function(err,resp){
      res.json(resp);
    })

})
server.post('/count/*',
  function(req, res){
    filter = [];
    var returnFilter = function(){
      if(filter != null){return _.compact(filter)}
      else { return {empty: null};}
    }
  var source = 'todos';
  if(req.params){
      source = req.params[0];
  }

  var query = getQuery(req.body.search,0);
  if(req.body.order == 'date'){
    query = getQuery(req.body.search,1);
  }



  if(req.body.search == '' ){
    query = { "match_all": {} };
    must_query = { "match_all": {} }
  }

    client.search(pruneEmpty({
    size: 0,
    index: 'cgr',
    body: {
      "query": {bool:
      {






         should: query
      }


    },
       "aggs": {
         "count_by_type": {
           "terms": {
             "field": "_type"
           }
         }
       }
    }
    }), function (error, response) {
        res.json(response);
        var time =  new Date();
        var ip = req.headers['iv-remote-address'] || req.connection.remoteAddress;
        client.index({
          index: 'search_log',
          type: 'search',

          body: {query: req.body.search, query_raw: req.body.search, ip: ip, time: time, fuente: source}
          }, function (error, response) {
            console.log(error)
          })
    })

})

server.post('/search/:source',
  function(req, res){
    filter = [];
    sort = {};

    var returnFilter = function(){
      if(filter != null){return _.compact(filter)}
      else { return {empty: null};}
    }

    if(req.body.order != 'score'){


      if(req.body.source == 'todos'){
          sort['fecha_documento'] = {'order':'desc',"ignore_unmapped" : true,"missing" : "_last"};
      }
      sort[req.body.date_name] = {'order':'desc',"ignore_unmapped" : true,"missing" : "_last"};
     // sort["_score"] = { "order": "desc" };


    }

    if(req.body.options.length > 0){

      var options = req.body.options;
      //console.log(req.body.options)
      filter  = options.map(function(i){
          var obj = {term: {}}
          if(i.type == 'force_obj'  ){
            if(i.value != 'not_show' && i.value != null){
              obj['term'] = {};
              obj['term'][i.field] = i.value;
              return obj;
            }
          } else if (i.type == 'category'){
              if(i.value != 'not_show' && i.value != null){
                obj = {};
                obj['multi_match'] = {
                  query:  "*"+i.value+"*",
                  fields: [i.field],
                  "fuzziness" : 1,
                  boost: 100,
                  type: 'phrase',
                  "prefix_length": 3,


                }
                return obj;
              }

          } else if (i.type == 'free'){
              if(i.value != '' && i.value != ' ' && i.value != undefined){
               obj['wildcard'] = {};
               obj['wildcard'][i.field] = '*'+i.value+'*';
                return obj;
              }

          } else if (i.type == 'date'){
              if(i.value != '' && i.value != ' '){
                obj = {range: {}};
                obj['range'][i.field] = {};
                obj['range'][i.field]['lt'] = i.value.lt;
                obj['range'][i.field]['gt'] = i.value.gt;



                return obj;
              }

          }

      })



      //return;

  }

  var query = getQuery(req.body.search, 1)
  let highlight = {
          "no_match_size": 200,


          fields : {
            "documento_completo" : {"fragment_size" : 200,"number_of_fragments" : 1},
            "texto" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "conclusiones" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "sentencia" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "vistas_fiscales" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "materia" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "nombre" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "descriptores" : {"fragment_size" : 200, "number_of_fragments" : 1},
            "parte" : {"fragment_size" : 200, "number_of_fragments" : 1}

          }
        };

    /**
    /*SCORE
    */
    if(req.body.source == 'web'){
      sort = {};
    }
    var search_res = pruneEmpty({
    index: 'cgr',
    type: req.body.source == 'todos'? 'contable,auditoria, dictamenes, boletin,cuentas,legislacion, web':req.body.source,
    size: 20,
    from: ((Number(req.body.page)) * 20),
    body: {
      sort: sort,
      query: {
      filtered: {
      query: {
        bool:
        {
            should: query
        }

      },
      filter: {
        query:{bool: {must:returnFilter(filter)}},

      }

      }},
      highlight : highlight
      }
    });

    /******
    /* FECHA
    */
    if(req.body.order == 'score' && req.body.source != 'web'){
          var sortname = {
              fecha_documento: {"scale": "50w","decay" : 0.5, "origin": "now"}};
          var sortnameclose = {
              fecha_documento: {"scale": "10w","decay" : 0.5, "origin": "now"}};

          if(req.body.source != 'todos'){
              sortname = {}
              sortnameclose = {}
              sortname[req.body.date_name] = {"scale": "50w","decay" : 0.5}
              sortnameclose[req.body.date_name] = {"scale": "20w","decay" : 0.5}
          }
    if(req.body.source == 'todos'){
          sort['fecha_documento'] = {'order':'desc',"ignore_unmapped" : true,"missing" : "_last"};
    }

    sort[req.body.date_name] = {'order':'desc',"ignore_unmapped" : true,"missing" : "_last"};
    query = getQuery(req.body.search,0);
    search_res = pruneEmpty({
    index: 'cgr',
    type: req.body.source == 'todos'? 'contable,auditoria, dictamenes, boletin,cuentas,legislacion, web':req.body.source,
    size: 20,
    from: ((Number(req.body.page)) * 20),
    body: {

      query: {
      filtered: {
      query: {
        bool:
        {
            should: query
        }

      },
      filter: {
        query:{bool: {must:returnFilter(filter)}},

      }

      }},
      highlight : highlight
      }
    });
    /*search_res = pruneEmpty({
    index: 'cgr',
    type: req.body.source == 'todos'? 'contable,auditoria, dictamenes, boletin,cuentas, legislacion, web':req.body.source,
    size: 20,
    from: ((Number(req.body.page)) * 20),
    body: {

      query: {
       function_score: {
        boost_mode: 'multiply',

        max_boost:  1.5,

        query: {
          filtered: {
            query: {
             bool:
              {
                should: query,

              }
            },
            filter: {
             query:{bool: {must:returnFilter(filter)}},

            }
         }
       },
       functions: [
         {
          "gauss": sortname



         }
       ]
      }
      },
      highlight : highlight,

      }
    });*/
  }
  client.search(search_res,function(err,resp){


      res.json(resp)

      console.log(JSON.stringify(search_res,null,4));
      console.log(err);



  })



})

function getQuery(search_string_enter, added_query){

  let search_string = search_string_enter;
  let search_string_full = search_string_enter;
  let search_string_no_numbers = search_string_enter;
  let add_query = []
  if(search_string_enter.split(' ').length > 1 ){
      search_string = '';
      search_string_no_numbers = '';

      search_string_enter.split(' ').map(function(i){
      if(i.length > 2 && _.indexOf(stopwords, i) == -1){
        //if(!isNumber(i)){
            search_string = search_string + ' ' +i;
        //}
        search_string_no_numbers = search_string_no_numbers + ' ' +i;
        if(added_query == 0){
          if(!isNumber(i) && i.length > 3){
            /*add_query.push({
                multi_match: {
                  query:  '*'+i.trim()+'*',
                  fields: ["_all","doc_id^3","n_dictamen^4"],
                  type: 'most_fields',
                  boost: 2,
                }})*/
          } else {
           /*add_query.push({
              multi_match: {
                query:  ''+i.trim()+'',
                fields: ["_all","doc_id^3","n_dictamen^4"],
                type: 'cross_fields',
                boost: 2,
              }})*/
        }
       }


      }
     });
    }
    var match_quote = /\'(.*?)\'/g;
    var match_double_quote = /\"(.*?)\"/g;
    var match;
    var phrases = [];
    match = match_quote.exec(search_string_enter);
    while (match != null) {
     phrases.push(match[1])
     match = match_quote.exec(search_string_enter);


    }
    match = match_double_quote.exec(search_string_enter);
    while (match != null) {
     phrases.push(match[1])
     match = match_double_quote.exec(search_string_enter);


    }
    let phrases_query = [];
    if(phrases.length > 0){
        phrases_query = phrases.map(function(e){
          search_string_full = search_string_full.replace(e.trim(), '');
          search_string = search_string_full.replace(e.trim(), '');
          return {multi_match: {
              query:  e.trim(),
              fields: ["doc_id^5","n_dictamen^4", "texto_espaniol_procesado"],
              "fuzziness" : 0,
              boost: 100,
              slop: 0,
              type: "phrase",
              max_expansions: 100,
              minimum_should_match: '100%',
              operator: 'and'



          }}
        })
    }
   if(search_string_no_numbers.trim() == '' || search_string_no_numbers.trim() == ' ' ){
    search_string_no_numbers = search_string;
   }
   if(search_string.trim() == ''){
    search_string = search_string_full;
   }
      var crossfield =    {multi_match: {
              query:  search_string.trim(),
              fields: ["_all","doc_id^3","n_dictamen^4", "texto_espaniol_procesado"],
              fuzziness: 0,
              boost: 5,
              type:"cross_fields",
              minimum_should_match: '100%',
              operator: 'and'




      }};
     if(added_query == 0){
     	crossfield =  {multi_match: {
              query:  search_string.trim(),
              fields: ["_all","doc_id^3","n_dictamen^4", "texto_espaniol_procesado"],
              fuzziness: 0,
              boost: 5,
              type:"cross_fields",
              minimum_should_match: '100%',
              operator: 'and'




      }}
     }
    var most_fields =  {multi_match: {
              query:  search_string.trim(),
              fields: ["_all","doc_id^3","n_dictamen^4", "texto_espaniol_procesado"],
              fuzziness: 0,
              boost: 5,
              minimum_should_match: '100%',
              operator: 'and'




      }};
     if(added_query == 0){
      most_fields =  {multi_match: {
              query:  search_string.trim(),
              fields: ["_all","doc_id^3","n_dictamen^4", "texto_espaniol_procesado"],
              fuzziness: 0,
              boost: 5,
              minimum_should_match: '100%',
              operator: 'and'




      }}
    }

    var query = [

          {multi_match: {
              query:  search_string_full.trim(),
              fields: ["_all","doc_id^5","n_dictamen^4", "texto_espaniol_procesado"],

              boost: 60,
              type: "phrase",

              operator: "and",
              slop: 1,
              minimum_should_match: '100%'



          }},



          crossfield,
          most_fields,
          add_query,
          phrases_query



  ]


    if(!isNaN(search_string) && added_query == 1){
    	query = [

          {multi_match: {
              query:  search_string_full.trim(),
              fields: ["doc_id^5","n_dictamen^4", "texto_espaniol_procesado"],
              "fuzziness" : 0,
              boost: 60,
              type: "phrase",
              max_expansions: 100,

              slop: 2,
              minimum_should_match: '3<70%'



          }},
          {wildcard: {
            "n_dictamen": {value: "*"+search_string_full.trim()+"*",boost: 90}
             }
          },
          {wildcard: {
            "doc_id": {value: "*"+search_string_full.trim()+"*",boost: 90}
             }
          }]
    }

  if(search_string_enter == '' ){
    query = { "match_all": {} };
    must_query = { "match_all": {} }
  }

  return query;
}
function pruneEmpty(obj) {
  return function prune(current) {
    _.forOwn(current, function (value, key) {
      if (_.isUndefined(value) || _.isNull(value) || _.isNaN(value) ||
        (_.isString(value) && _.isEmpty(value)) ||
        (_.isObject(value) && _.isEmpty(prune(value)))) {

        delete current[key];
      }
    });
    // remove any leftover undefined values from the delete
    // operation on an array
    if (_.isArray(current)) _.pull(current, undefined);

    return current;

  }(_.cloneDeep(obj));  // Do not modify the original object, create a clone instead
}


server.get('/delete/now',
  function(req, res){

  client.search({
  index: 'cgr',
  type: 'jurisprudencia',
  size: 2000,
  suggestMode: "always",
  body: {
    query: {
      multi_match: {
        query:  "a",
        fields: ["_all"],
        "fuzziness" : 2,
        "prefix_length": 0,
        "boost" : 1.2,
        "minimum_should_match": "10%"

      }
    }
  }
},function(err,resp){
    resp.hits.hits.forEach(function(i,k){
      client.delete({
        index: 'cgr',
        type: 'jurisprudencia',
        id: i._id
      })
    })



  })


})





server.listen(port, function(){
  /* eslint-disable no-console */
  console.log('The server is running at http://localhost:'+port+'/');
});
