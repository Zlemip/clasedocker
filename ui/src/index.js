import React,{Component } from 'react';
import ReactDOM from 'react-dom';
import Search from './components/Search';

function WidgetCGR (opts) {
  if (!(this instanceof WidgetCGR)) return new WidgetCGR(opts);

}


class Modal extends Component {
  render(){
    return (
        <div style={{position: 'fixed',height: '100vh',width: '100vw',zIndex: 9999}}>
          <h3 style={{color: '#fff',textAlign:'center',position:'absolute',zIndex: 99999, width: '100%', top: '60px',fontSize: '30px'}}>{this.props.title}</h3>
          <h4 style={{color: '#fff',textAlign:'center',position:'absolute',zIndex: 99999, width: '100%', top: '90px', borderBottom: '0px'}}>{this.props.subtitle}</h4>
          {this.props.children}
        </div>
    )
  }
}


WidgetCGR.prototype.create = function(opts){

  const url = window.location.href ;
  var styleId =  'style-widget';
  document.getElementById(opts.mount)
  var style =  document.createElement('link');
  style.id = styleId;
  style.setAttribute("rel", "stylesheet")
  style.setAttribute("type", "text/css")
  style.setAttribute("href", "http://testing.contraloria.cl/buscador/css/widget.min.css")
  var script_new =  document.createElement('script');
  script_new.id = 'html2canvas';

  script_new.setAttribute("type", "text/javascript")
  script_new.setAttribute("src", "//html2canvas.hertzen.com/build/html2canvas.js")
  var script_new =  document.createElement('script');
  script_new.id = 'html2canvas';

  script_new.setAttribute("type", "text/javascript")
  script_new.setAttribute("src", "http://testing.contraloria.cl/buscador/js/jspdf.min.js")
  document.getElementsByTagName('head')[0].appendChild(style);
  if(opts.format == 'modal'){
    var alertaDiv =  document.createElement('div');
    alertaDiv.id = opts.mount;
    document.body.insertBefore(alertaDiv,document.body.firstChild);
  }
  //render(url, site, data_api);
  /*axios.get(serviceUrl + '/sites/'+site.id)
  .then(({data})=>{
    render(url, site, data);
  })
  .catch((err)=>{
    console.log('The site you requested might not be in our records: '+err);
  });*/
  render(opts)
};


const render = (opts)=>{
  if(opts.format == 'modal'){
    ReactDOM.render(
      <Modal title={opts.title} subtitle={opts.subtitle}><Search  {...opts} /></Modal>,
      document.getElementById(opts.mount)
    );
  } else {
    ReactDOM.render(
      <Search  {...opts} />,
      document.getElementById(opts.mount)
    );
  }

}

window.WidgetCGR = WidgetCGR;
module.exports = WidgetCGR;
