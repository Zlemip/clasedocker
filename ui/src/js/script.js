
/******** ALTERNAR ICONOS DEL ACORDEÓN **********/
$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".fa-bars").removeClass("fa-bars").addClass("fa-times");
	}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".fa-times").removeClass("fa-times").addClass("fa-bars");
});

/******** CAMBIAR CABECERA AL HACER SCROLL **********/
(function(a){if(a)a.events={DOMNodeInserted:function(b){var b=b.target,c=b.nodeName;b.nodeType==1&&(/link/i.test(c)?a.link(b):/style/i.test(c)?a.styleElement(b):b.hasAttribute("style")&&a.styleAttribute(b))},DOMAttrModified:function(b){b.attrName==="style"&&(document.removeEventListener("DOMAttrModified",a.events.DOMAttrModified,false),a.styleAttribute(b.target),document.addEventListener("DOMAttrModified",a.events.DOMAttrModified,false))}},document.addEventListener("DOMContentLoaded",function(){document.addEventListener("DOMNodeInserted",
a.events.DOMNodeInserted,false);document.addEventListener("DOMAttrModified",a.events.DOMAttrModified,false)},false)})(window.StyleFix);
(function(a){if(a&&window.CSSStyleDeclaration)for(var b=0;b<a.properties.length;b++){var c=StyleFix.camelCase(a.properties[b]),d=a.prefixProperty(c),e=CSSStyleDeclaration.prototype,f=function(a){return function(){return this[a]}}(d),d=function(a){return function(b){this[a]=b}}(d);Object.defineProperty?Object.defineProperty(e,c,{get:f,set:d,enumerable:true,configurable:true}):e.__defineGetter__&&(e.__defineGetter__(c,f),e.__defineSetter__(c,d))}})(window.PrefixFree);

$(window).scroll(function () {
	$(this).scrollTop() < 150 ?
      $('.header-wrapper').removeClass('short'):
			$('.header-wrapper').addClass('short');
});

/******** CAMBIAR TAMAÑO FUENTE (ACCESIBILIDAD) **********/
valorFuente = 1;
function cssUp() {
	if(valorFuente == 0){
		document.getElementById("cssFonts").href="css/fonts.css";
		valorFuente = 1;
	}else if(valorFuente == 1){
		document.getElementById("cssFonts").href="css/fonts-big.css";
		valorFuente = 2;
	}
}
function cssDown() {
	if(valorFuente == 1){
		document.getElementById("cssFonts").href="css/fonts-small.css";
		valorFuente = 0;
	}else if(valorFuente == 2){
		document.getElementById("cssFonts").href="css/fonts.css";
		valorFuente = 1;

	}
}

/******** MODO TEXTO (ACCESIBILIDAD) **********/
function textOnly() {
	document.getElementById("cssColor").href="css/color-textonly.css";

	var elements = document.getElementsByTagName('img');

	for(var i in elements) {
	  //elements[i].style.opacity = 0.3;
	  elements[i].src="noimage";
	}
}

/******** RESEATEAR ESTILOS (ACCESIBILIDAD) **********/
function reset() {
	document.getElementById("cssFonts").href="css/fonts.css";
}

/******** AL CARGAR LA PÁGINA **********/
$(document).ready(function(){

	/******** FULLPAGE **********/
	var sections = $('#pageContainer').find("section");

	var fpAnchors = ['inicio', 'actualidad', 'ciudadano', 'instituciones', 'funcionarios', 'municipalidades'];

	try {
	$('#pageContainer').fullpage({
		sectionSelector: '.vertical-scrolling',
		navigation: true,
		css3: true,
		navigationTooltips: ["Inicio","Actualidad","Ciudadano","Entidades Públicas","Funcionarios Públicos","Municipalidades"],
		anchors: fpAnchors,
		scrollBar: true,
		afterLoad: function(anchorLink, index) {
			if (index < 6) {
				var nextSectionTitle = $(sections[index]).data('section-title');
				$('.fixedMenu .actualidad-link .content span').html(nextSectionTitle);
				$('.fixedMenu .actualidad-link .content a').attr("href", "#"+fpAnchors[index]);
				$('.fixedMenu .actualidad-link .content a img').removeClass('rotate');
			}else if (index == 6) {
				var nextSectionTitle = $(sections[0]).data('section-title');
				$('.fixedMenu .actualidad-link .content span').html(nextSectionTitle);
				$('.fixedMenu .actualidad-link .content a').attr("href", "#"+fpAnchors[0]);
				$('.fixedMenu .actualidad-link .content a img').addClass('rotate');
			}
			if (index > 1) {
				$('.header-wrapper').addClass('short');
				$('.fixedMenu').addClass('short');
			}else {
				$('.header-wrapper').removeClass('short');
				$('.fixedMenu').removeClass('short');
			}

			$('.fixedMenu .actualidad-link .content').fadeIn();
		},

		onLeave: function(index, nextIndex, direction) {

		}
	});

	/******** BANNER SLIDER VEGAS **********/
	$("#banner").vegas({
		slides: [
			{ src: "img/banner/slide1.jpg" },
			{ src: "img/banner/slide2.jpg" },
			{ src: "img/banner/slide3.jpg" },
			{ src: "img/banner/slide4.jpg" },
			{ src: "img/banner/slide5.jpg" },
			{ src: "img/banner/slide6.jpg" },
			{ src: "img/banner/slide7.jpg" },
			{ src: "img/banner/slide8.jpg" }
		]
	});
} catch(err){
	console.log(err)
}


});

$(document).ready(function(){
	
	var widgetCGR = new WidgetCGR();
	widgetCGR.create({
		mount: 'widget-cgr',
		api: '//localhost:8090',
		format: 'bigHome', // 'inline' : Se muestra como objeto inline // 'modal' genera un boton que gatilla el objeto // 'page' toma la pagina como objeto
		sources: [], // permite seleccionar las fuentes, si se encuentra vacio trae todas
		filter: []
	});


	$('.municipalidad-call-search-auditoria').click(function(e){
		e.preventDefault()
		var widgetCGR = new WidgetCGR();
		widgetCGR.create({
			mount: 'muni-search',
			api: '//testing.contraloria.cl/apibuscador',
			format: 'modal', // 'inline' : Se muestra como objeto inline // 'modal' genera un boton que gatilla el objeto // 'page' toma la pagina como objeto
			sources: ['auditoria'], // permite seleccionar las fuentes, si se encuentra vacio trae todas
			filter: [],
			combo: [{
						key:'nivel',
						value:'Central',
						children: [
							{key: 'área', value:'Defensa Nacional'}
						]}]
		});
		return false;
	})



	$('.municipalidad-call-search-dictamenes').click(function(e){
		e.preventDefault()
		var widgetCGR = new WidgetCGR();
		widgetCGR.create({
			mount: 'muni-search',
			title: 'Jurisprudencia',
			subtitle: 'Buscando en Municipales',
			api: '172.30.20.42',
			format: 'modal', // 'inline' : Se muestra como objeto inline // 'modal' genera un boton que gatilla el objeto // 'page' toma la pagina como objeto
			sources: ['dictamenes'], // permite seleccionar las fuentes, si se encuentra vacio trae todas
			filter: {key: 'Materia',value: 'Municipales'} // Permite fijar la busqueda avanzada

		});
		return false;
	})

	$('.search-top').focus(function(e){
		console.log('yes')
		e.preventDefault()
		var widgetCGR = new WidgetCGR();
		widgetCGR.create({
			mount: 'muni-search',
			api: '52.41.60.181:8080',
			format: 'modal', // 'inline' : Se muestra como objeto inline // 'modal' genera un boton que gatilla el objeto // 'page' toma la pagina como objeto
			sources: [], // permite seleccionar las fuentes, si se encuentra vacio trae todas
			filter: {} // Permite fijar la busqueda avanzada

		});
		return false;
	})

})
